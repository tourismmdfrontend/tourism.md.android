# Tourism.MD

One Paragraph of project description goes here

## Rest API Schema
* [POST] api/user/register

    id:int, username:String, password:String, email:String

* [GET] api/user/login

    username:String, password:String

* [GET] api/admin/login

    username:String, password:String

* [GET] api/poi/list

    void

* [GET] api/poi/{id}

    id:int